angular.module('app').controller('Vocabulary', function ($window, $scope, $http, $compile, $uibModal, AnnotationRESTService, AnnotationsService, BindingsService, AnnotationBoxService,appConfig) {

    //flags
    /**
     * File for annotation was loaded
     * @type {boolean}
     */
    $scope.fileLoaded = false;
    /**
     * Something loads from server
     * @type {boolean}
     */
    $scope.loader = false;

    /**
     * Annotation be searched
     * @type {boolean}
     */
    $scope.search = false;

    /**
     * Annotation is editing
     * @type {boolean}
     */
    $scope.edit = false;

    /**
     * Annotation was saved for copy
     * @type {boolean}
     */
    $scope.save = false;


    /**
     * Saves data from search form
     * @type {{search: string, indexDataset: string, luceneIndex: string}}
     */
    $scope.searchForm = {
        search: '',//word to search
        indexDataset: 'aat',//Getty Vocabularies
        luceneIndex: 'brief'//for Brief is used predicate luc:term, for Full is used predicate luc:text
    };

    /**
     * Variable for checkboxes
     * @type {{words: Array}}
     */
    $scope.formData = {
        words: []
    };

    /**
     * Bindings list for checkboxes
     * @type {[string]}
     */
    $scope.bindings;

    /**
     * Variable for saving annotation for transferring it to another word
     * @type {{id: number, prefix: string, suffix: string, exact: string, wordToSearch: string, words: Array, bindings: Array}}
     */
    $scope.savedAnnotation;

    /**
     * Saving annotation id for annotation edit
     * @type {id: number}
     */
    $scope.editAnnotationID;

    /**
     * Text for annotating
     * @type {string}
     */
    $scope.text = 'Download the file';

    /**
     * Variable for files uploading
     * @type {Array}
     */
    $scope.files = [];

    /**
     * Saves found word. Is showed before list of checkboxes
     * @type {string}
     */
    $scope.findString;

    /**
     * Saves alerts to show on page
     * @type {[msg: string, type: string]}
     */
    $scope.alerts = [];

	/**
	 * For download file
	 * @type {string}
	 */
	$scope.url = appConfig.url;


    /**
     * Copies annotation for transferring it to another word
     *
     * @param id in annotations saved in AnnotationsService
     */
    $scope.copyAnnotation = function(id) {
        console.log("copyAnnotation");
        $scope.savedAnnotation = AnnotationsService.getAnnotation(id);
		$scope.savedAnnotation.words = AnnotationsService.getAnnotation(id).words;
		$scope.savedAnnotation.bindings = AnnotationsService.getAnnotation(id).bindings;
        console.log($scope.savedAnnotation);

        //$scope.formData was changed for annotation edit; return to default
        $scope.formData = {
            words: []
        };
        $scope.save = true;
        $scope.edit = false;
    };

    /**
     * Pastes saved annotation to selected word
     */
    $scope.pasteAnnotation = function(){
        console.log("pasteAnnotation");
		console.log($scope.savedAnnotation);
        if (!window.getSelection().isCollapsed) {
            var rnd = window.getSelection().getRangeAt(0);
            var newAnnotation = AnnotationsService.createAnnotation(rnd, $scope.savedAnnotation.words, true);
            if (!angular.isUndefined(newAnnotation)) {
				newAnnotation.wordToSearch = $scope.savedAnnotation.wordToSearch;

				AnnotationRESTService.add(newAnnotation).then(function (response) {
					if (response.data >= 0) {
						//save annotation in service
						AnnotationsService.add(newAnnotation);

						AnnotationBoxService.updateBox(newAnnotation.id, rnd, true);
						$compile(angular.element(document.querySelector('#text-parent')))($scope);
					}
				}, function(response) {
					console.log("pasteAnnotation error");
					console.log(response);
					$scope.addAlert("danger", "Paste annotation error");
				});

				$scope.savedAnnotation = null;
				$scope.searchForm = {
					search: '',
					indexDataset: 'aat',
					luceneIndex: 'brief',
					words: []
				};
				$scope.clearSelection();

				$scope.save = false;
            } else {
				$scope.addAlert("warning", "An error occurred while copying the annotation. " +
					"Try to copy the annotation one more time.");
			}
        } else {
            $scope.addAlert("warning", "Text for annotation was not selected");
        }
    };

    /**
     * By clicking the form button
     * Saves the annotation on the server
     */
    $scope.addAnnotation = function(){
        console.log("addAnnotation");

		//clears form
		$scope.searchForm = {
			search: '',
			indexDataset: 'aat',
			luceneIndex: 'brief',
			words: []
		};

		//clear selection
		$scope.clearSelection();

        $scope.findString = '';

        //if was put at least one checkbox
        if ($scope.formData.words.length > 0) {
            //gets all preselected words
            var elements = document.getElementsByClassName('temp-selection');

            angular.forEach(elements, function(el){
                var annotation = AnnotationsService.createAnnotation(el, $scope.formData.words);
				if (!angular.isUndefined(annotation)) {
					AnnotationRESTService.add(annotation).then(function (response) {
						if (response.data >= 0) {
							//save annotation in service
							AnnotationsService.add(annotation);

							AnnotationBoxService.updateBox(annotation.id, el);
							$compile(angular.element(document.querySelector('#text-parent')))($scope);
						}
					}, function (response) {
						console.log("addAnnotation error");
						console.log(response);
						$scope.addAlert("danger", "Add annotation error");
					});
				}
            });
            //clear selected checkboxes
            $scope.formData.words = [];
        } else {
            AnnotationBoxService.delAllRepeats();
            $compile(angular.element( document.querySelector( '#text-parent' ) ))($scope);

            $scope.addAlert("warning", "No bindings was selected");
        }
        // hide form
        $scope.search = false;
    };

    /**
     * Sends annotation changes to the server
     *
     * @param id of editing annotation saved in AnnotationsService
     */
    $scope.editAnnotationOnServer = function(id) {
		//removes the ability to insert a previously copied annotation
        $scope.save = false;

        $scope.findString = '';
        if (id >= 0) {
            // edit annotation
            if ($scope.formData.words.length > 0) {
                AnnotationsService.updateAnnotation(id, $scope.formData);

                var annotation = AnnotationsService.getAnnotation(id);

                AnnotationRESTService.edit(annotation).then(function (response) {
                    $compile(angular.element( document.querySelector( '#text-parent' ) ))($scope);

                    $scope.formData.words = [];
                }, function(response) {
                    console.log("editAnnotationOnServer edit annotation error");
                    console.log(response);
                    $scope.addAlert("danger", "editAnnotationOnServer edit annotation error");
                });

            } else { // del annotation
                var el = document.querySelectorAll('[annotationid="' + id + '"]');

                AnnotationsService.delAnnotation(id);
                AnnotationRESTService.del(id).then(function () {
                    AnnotationBoxService.delBox(el[0]);
                    $compile(angular.element( document.querySelector( '#text-parent' ) ))($scope);
                }, function(response) {
                    console.log("editAnnotationOnServer delete annotation error");
                    console.log(response);
                    $scope.addAlert("danger", "editAnnotationOnServer delete annotation error");
                });
            }
        } else {
                $scope.addAlert("danger", "Annotation id is not correct");
        }
        $scope.edit = false;
    };

    /**
     * Edits annotation. on click on popover
     *
     * @param id of annotation
     */
    $scope.editAnnotation = function(id) {
        $scope.searchForm = {
            search: '',
            indexDataset: 'aat',
            luceneIndex: 'brief',
            words: []
        };
        $scope.clearSelection();

        var annotation = AnnotationsService.getAnnotation(id);

        if(annotation !== null) {
            $scope.findString = annotation.wordToSearch;
            $scope.loader = true;

            $scope.exact = annotation.wordToSearch;

            // search all words.

            $http({
                method: 'GET', url: appConfig.url+"entry-point/search?search=" + $scope.exact
            }).then(function (response) {
                $scope.loader = false;

                //check all words from annotation
                $scope.formData.words = annotation.words;

                var bindings = BindingsService.insertHumanURI(response.data.results.bindings);
                $scope.bindings = bindings;
                //just in case. theoretically nothing new will add
                BindingsService.addBindingsList(bindings);

                $scope.editAnnotationID = id;

                $scope.edit = true;
                $scope.search = false;
             }, function(response) {
                console.log("editAnnotation  error");
                console.log(response);
                $scope.addAlert("danger", "editAnnotation error");
            });
        } else {
            $scope.addAlert("danger", "Annotation id is not correct");
        }

    };


    /**
     * Deletes all selection
     */
    $scope.clearSelection = function() {
        try {
            window.getSelection().removeAllRanges();
        } catch (e) {
            //IE8-
            document.selection.empty();
        }

        //$scope.searchForm.search = '';
    };

    /**
     * On search selected all word repeats. This function deletes a repeat by clicking
     *
     * @param $event
     */
    $scope.delRepeat = function($event) {
        AnnotationBoxService.delRepeat($event.currentTarget);
    };


    /**
     * On tapping search
     * Searches a word from the form $scope.searchForm on the server, saves the list in BindingsService
     */
    $scope.searchSelection = function() {
        if ($scope.searchForm.search.length > 0) {

            $scope.loader = true;
            $scope.search = true;
            $scope.edit = false;

            AnnotationBoxService.delAllRepeats();
            $compile(angular.element(document.querySelector('#text-parent')))($scope);

            $scope.findString = $scope.searchForm.search;

            AnnotationBoxService.selectRepeats($scope.findString, document.getElementById('text-parent'));
            $compile(angular.element(document.querySelector('#text-parent')))($scope);

            //get bindings und save them in $scope.bindings
            AnnotationRESTService.getBindings($scope.searchForm).then(function (response) {
				var bindings = BindingsService.insertHumanURI(response.data.results.bindings);
				BindingsService.addBindingsList(bindings);
				$scope.bindings = bindings;
				$scope.loader = false;

            	if (bindings.length <= 0) {
					AnnotationBoxService.delAllRepeats();
				}
            }, function (response) {
                console.log("searchSelection  error");
                console.log(response);
                $scope.addAlert("danger", "searchSelection error");
            });
        } else {
            $scope.addAlert("warning", "Select text for annotation");
        }

    };

    /**
     * Uploads file for annotation oder 2 files: text and annotations
     */
    $scope.uploadFile = function() {
		$scope.loader = true;


        var uploadUrl = appConfig.url + "entry-point/files";

        var fd = new FormData();

        for (var i = 0; i < $scope.files.length; i++)
            fd.append('files', $scope.files[i]);

        $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }
        ).then(function (response) {
			//all variables to default
			$scope.fileLoaded = false;
			$scope.loader = false;
			$scope.search = false;
			$scope.edit = false;
			$scope.save = false;
			$scope.searchForm = {
				search: '',//word to search
				indexDataset: 'aat',//Getty Vocabularies
				luceneIndex: 'brief'//for Brief is used predicate luc:term, for Full is used predicate luc:text
			};
			$scope.formData = {
				words: []
			};


			$scope.bindings = [];
			$scope.files = [];
			$scope.findString = '';
			$scope.alerts = [];


			var textChildren = [];

                angular.forEach(response.data, function(text) {
                    if (text.bindings) {
                        angular.forEach(text.bindings, function (binding) {
                            var bindingWithHumanURI = BindingsService.insertHumanURI(binding.results.bindings[0]);
                            BindingsService.addBinding(bindingWithHumanURI);
                        });
                    }

                    var textChild = {
                        text: text.text,
                        filename: text.filename,
                        annotationsId: [],
                        bindings: text.bindings
                    };

                    if (angular.isArray(text.annotation)){
                        angular.forEach(text.annotation, function(annotationFile){
                            textChild.annotationsId.push(annotationFile.id);
                        });

                    }
                    textChildren.push(textChild);
                });
                AnnotationsService.saveServerAnnotations(response.data);
                return textChildren;
            }, function (response) {
				console.log("uploadFile error");
				console.log(response);
				$scope.addAlert("danger", "upload file error");
			}).then(function(textChildren){
                var textParent = angular.element(document.getElementById("text-parent"));
                textParent.empty();

                angular.forEach(textChildren, function(child) {
                    var annotatedText = child.text;

                    if (child.annotationsId.length > 0) {
                        angular.forEach(child.annotationsId, function (id) {
                            var annotation = AnnotationsService.getAnnotation(id);
                            var annotatedTextTile = annotation.prefix + annotation.exact + annotation.suffix;
                            var box = annotation.prefix + '<annotation-box annotationId="' + annotation.id +
                                '" edit="editAnnotation(' + annotation.id + ', $event)"></annotation-box>' + annotation.suffix;

                            annotatedText = annotatedText.replace(annotatedTextTile, box);
						});
                        //it need a tag in start, for jqLite
						annotatedText = "<span></span>" + annotatedText;

                    } else {
                        annotatedText = document.createTextNode(child.text);
                    }
					textParent.append($compile(annotatedText)($scope));
					$scope.loader = false;
                });

                $scope.fileLoaded = true;
            });
    };

    /**
     * Adds alert
     * @param {string} type sets out bootstrap class; primary, success, info, warning, danger
     * @param {string} msg message for alert
     */
	$scope.addAlert = function(type, msg) {
        if (msg.length > 0 && $scope.isUnicAlert(msg)) {
            $scope.alerts.push({type: type, msg: msg});
        }
    };

	/**
	 * Check, if it is allert with the same message in alerts
	 *
	 * @param msg text of message
	 * @returns {boolean} true, if it is no alerts with the same message
	 */
	$scope.isUnicAlert = function(msg) {
		for (var i = 0; i <  $scope.alerts.length; i++) {
			if ($scope.alerts[i].msg === msg) return false;
		}

		return true;
	}

    /**
     * Closes alert by index
     * @param index
     */
    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };


});