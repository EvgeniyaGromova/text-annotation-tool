angular.module('app').directive('ngGetSelection', function ($timeout) {
    var text = '';

    function getSelectedText() {
        var text = "";

        if (typeof window.getSelection !== "undefined") {
            if (!window.getSelection().isCollapsed)
				if (window.getSelection().getRangeAt(0).commonAncestorContainer.parentNode.id == "text-parent")
					text = window.getSelection().toString();
        } else if (typeof document.selection !== "undefined" && document.selection.type === "Text") {
            text = document.selection.createRange().text;
        }
        return text;
    }

    return {
        restrict: 'A',
        scope: {
            ngGetSelection: '='
        },
        link: function (scope, element) {
            $timeout(function getSelection() {
                var newText = getSelectedText();

                if(newText.length > 0){
                    if (text !== newText) {
                        text = newText;
                        element.val(newText);
                        scope.ngGetSelection = newText;
                    }
                }

                $timeout(getSelection, 50);
            }, 50);

        }
    };
});
/*
angular.module('app').directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
*/

angular.module('app').directive('annotationBox', function(AnnotationsService, BindingsService) {
    return {
        restrict: 'E',
        scope: {
            annotationId: '@',
            edit: '&'
        },

        template: function($scope, $element){
           // console.log("annotationBox template")
            var annotation = AnnotationsService.getAnnotation($element.annotationid);

            var word = "";

            if(annotation === null) {
                $scope.remove();
            } else {
                word = annotation.exact
            }

            var popoverArray = [];

            if (annotation !== null) {
               // console.log("annotation !== null ");
               // console.log("annotation.bindings.length " + annotation.bindings.length);
              //  console.log(annotation);
              //  console.log(annotation.words);
                for (var i = 0; i < annotation.bindings.length; i++) {
                    popoverArray.push(annotation.bindings[i].Term.value);
                    //console.log(annotation.bindings[i].Term.value);
                }
            }

            var popover =  popoverArray.join(', ');
            //console.log("dir " + popover);

            // Generate string content that will be used by the template
            // function to replace the innerHTML with or replace the
            // complete markup with in case of 'replace:true'
            return '<span ng-click="edit()" ' +
                   ' style="pointer-events: auto; cursor: pointer;" ' +
                   ' popover-placement="top" ' +
                   ' popover-trigger="\'mouseenter\'" ' +
                   ' uib-popover="' + popover + '">' + word + '</span>';
        },

        controller: function ($scope, $element, $attrs) {
        },
    };
});

angular.module('app').directive('ngFileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var isMultiple = attrs.multiple;
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    if (isMultiple) {
                        modelSetter(scope, element[0].files);
                    } else {
                        modelSetter(scope, element[0].files[0]);
                    }
                });
            });
        }
    };
}]);

angular.module('app').directive('stickyColumn', function ($window) {
    return {
        restrict: 'AE',
        link: function (scope, element, attrs) {
            var header = angular.element(element);

            angular.element($window).bind("scroll", function () {
                var fromTop = $window.pageYOffset;
                var body = angular.element(document).find('body');

                body.toggleClass('down', (fromTop > 400));
                if (fromTop >= 80) {
                    angular.element(header[0]).css("top", '0');
                    angular.element(header[0]).css("position", 'fixed');
                    angular.element(header[0]).css("width", 'inherit');
                } else {
                    angular.element(header[0]).css("top", '');
                    angular.element(header[0]).css("position", '');
                    angular.element(header[0]).css("width", '');
                }
            });
        }
    };
});