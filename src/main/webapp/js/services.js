/*angular.module('app').service('fileUpload', ['$http', function ($http) {
    return {
        uploadFileToUrl: function (file, uploadUrl) {
            this.text = '';
            var fd = new FormData();
            fd.append('file', file);

            $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }
            )
                .then(function (response) {
                    return response.data;
                })
        }
    }
}]);*/

/**
 * Annotations list sevice. Add, del, search
 */
angular.module('app').service('AnnotationsService', function(BindingsService, AnnotationRESTService) {
    /** Annotation format
     * var annotation = {
     *       id: '',
     *       prefix: '',
     *       suffix: '',
     *       exact: '',
     *       wordToSearch: '',
     *       words: [], //getty urls
     *       bindings: [//getty object
     *          {
     *              Parents: {
     *                  type: '',
     *                  value:''
     *               },
     *              ScopeNote: {
     *                  xml:lang: '',
     *                  type: '',
     *                  value: ''
     *              },
     *              Subject: {
     *                  type: "uri",
     *                  value: "http://vocab.getty.edu/aat/300417284"
     *              },
     *              Term: {
     *                  xml:lang: "en",
     *                  type: "literal",
     *                  value: "flat file cabinets"
     *             }
     *          }, ...
     *       ]
     *  };
     */
    this.annotations = [];
    this.annotationLastId = -1;

    /**
     * Add new annotation in list
     *
     * @param annotation new annotaton
     * @returns {Array | null} annotations list; null if annotation was in list
     */
    this.add = function(annotation) {
        if (this.search(annotation.id) === -1) {
            this.annotations.push(annotation);
            return this.annotations;
        } else {
            return null;
        }
    };

    /**
     * Deletes annotation by annotation id
     *
     * @param id of annotation for del
     * @returns {Array} annotations list {this.annotations|Array}
     */
    this.delAnnotation = function(id) {
        var annotationId = this.search(id);

        if(annotationId >= 0) {
            this.annotations.splice(annotationId, 1); //from position annotationId, delete 1 item
        }

        return this.annotations;
    };

    /**
     * Search annotation index in this.annotations
     *
     * @param id of annotation
     * @returns {number} index of annotation with id; -1 if not found
     */
    this.search = function(id) {
        var i;

        for (i = 0; i < this.annotations.length; i++) {
            if (this.annotations[i].id == id) {
                return i;
            }
        }

        return -1;
    };

    /**
     * Gets an annotation by id
     *
     * @param id annotation id
     * @returns {{id: number, prefix: string, suffix: string, exact: string, wordToSearch: string, words: Array, bindings: Array} | null} annotation found by id
     */
    this.getAnnotation = function(id) {
        var annotationID = this.search(id);

        if (annotationID != -1) {
            return this.annotations[annotationID];
        } else {
            return null;
        }
    };

    /**
     * Creates and returns an annotation
     *
     * @param el is dom node or Range
     * @param words {Array} von bindings
     * @param isRange if true - el is Range; isRange is true if an annotation is copied
     * @returns {{id: number, prefix: string, suffix: string, exact: string, wordToSearch: string, words: Array, bindings: Array}}
     */
     this.createAnnotation = function(el, words, isRange) {
         if (isRange === undefined) {
             isRange = false;
         }

         var annotation = {
             id: -1,
             prefix: '',
             suffix: '',
             exact: '',
             wordToSearch:'',
             words: [],
             bindings: []
         };

         //set annotation id
         /*if (this.annotationLastId < 0) {
             this.annotationLastId = AnnotationRESTService.getAnnotationLastId();
         }*/
         annotation.id = this.annotationLastId + 1;
         this.annotationLastId = this.annotationLastId + 1;




         // variables for prefix and suffix
         if (isRange) {
             var str = el.commonAncestorContainer.textContent;
             var strStart = el.startOffset;
             var strEnd = el.endOffset;
         }
         var offset = 50; //length of prefix and suffix

         //search prefix
         if (isRange) {
             if (strStart > (strStart - offset)){
                 annotation.prefix = str.substring(strStart - offset, strStart);
             } else {
                 annotation.prefix = str.substring(0, strStart);
             }
         } else {
             if (el.previousSibling != null) {
                 var prev = el.previousSibling.textContent;
                 if (prev.length > offset) {
                     annotation.prefix = prev.substr(prev.length - offset, offset);
                 } else {
                     annotation.prefix = prev;
                 }
             }
         }

         //search suffix
         if (isRange) {
             if (str.length > (strEnd + offset)){
                 annotation.suffix = str.substring(strEnd, strEnd + offset);
             } else {
                 annotation.suffix = str.substring(strEnd, str.length);
             }
         } else {
             if (el.nextSibling != null) {
                 var next = el.nextSibling.textContent;
                 if (next.length > offset) {
                     annotation.suffix = next.substr(0, offset);
                 } else {
                     annotation.suffix = next;
                 }
             }
         }



         if (isRange) {
             // isRange is true if an annotation is copied; annotation.wordToSearch already determined
             annotation.exact = el.toString();
         } else {
             annotation.exact = el.textContent;
             annotation.wordToSearch = el.textContent;
         }


         annotation.words = words;

         // add bindings
         var myBindings = [];
         var binding;
         for (var i = 0; i < annotation.words.length; i++) {
             binding = BindingsService.getBinding(annotation.words[i]);
             if (binding !== null) {
                 myBindings.push(binding);
             }
         }
         annotation.bindings = myBindings;

         if (annotation.exact.length > 0)
         	return annotation;
    };

    /**
     * Updates an annotation words and bindings by id and formData
     *
     * @param id of annotation
     * @param formData
     */
    this.updateAnnotation = function(id, formData) {
        var annotationId = this.search(id);

        this.annotations[annotationId].words = formData.words;

        var myBindings = [];
        var binding;
        for (var i = 0; i < this.annotations[annotationId].words.length; i++) {
            binding = BindingsService.getBinding(this.annotations[annotationId].words[i]);
            if (binding !== null) {
                myBindings.push(binding);
            }
        }

        this.annotations[annotationId].bindings = myBindings;
    };

    /**
     * Selects annotations downloaded from the file
     *
     * @param serverData
     */
    this.saveServerAnnotations = function(serverData) {
        this.annotations = [];
        //var maxId = -1;
        var annotationsService = this;
        angular.forEach(serverData, function(data) {
            if (angular.isArray(data.annotation)) {
                angular.forEach(data.annotation, function(annotationFile) {
                    var annotation = {
                        id: annotationFile.id,
                        prefix: (angular.isUndefined(annotationFile.target[0].selector.prefix) ?
                            '' : annotationFile.target[0].selector.prefix),
                        suffix: (angular.isUndefined(annotationFile.target[0].selector.suffix) ?
                            '' : annotationFile.target[0].selector.suffix),
                        exact: annotationFile.target[0].selector.exact,
                        wordToSearch: annotationFile.target[0].selector.exact,
                        words: [],
                        bindings: []
                    };
                   // if (maxId < annotationFile.id) maxId = annotationFile.id;


                    annotationsService.add(annotation);

                    angular.forEach(annotationFile.target, function(target) {
                        var annotatedTextTile = annotation.prefix + annotation.exact +annotation.suffix;
                        var binding = BindingsService.getBinding(target.source);
                        if (binding !== null) {
                            annotationsService.pushWord(annotationFile.id, target.source);
                            annotationsService.pushBinding(annotationFile.id, binding);
                        } else {
                            AnnotationRESTService.getBindingByURI(target.source).then(function (response) {
                                var binding = BindingsService.insertHumanURI(response.data.results.bindings[0]);
                                annotationsService.pushWord(annotationFile.id, target.source);
                                annotationsService.pushBinding(annotationFile.id, binding);

                                BindingsService.addBinding(binding);
                            });
                        }
                    });


                });
            }
        });
        this.setAnnotationLastId();
    };

    this.pushWord = function(id, word) {
        var annId = this.search(id);
        var push = true;

        for (var i = 0; i < this.annotations[annId].words.length; i++) {
            if (this.annotations[annId].words[i] == word) {
                push = false;
            }
        }

        if (push) {
            this.annotations[annId].words.push(word);
        }
    };

    this.pushBinding = function(id, binding) {
        var annId = this.search(id);
        var push = true;

        for (var i = 0; i < this.annotations[annId].bindings.length; i++) {
            if (this.annotations[annId].bindings[i].Subject.value == binding.Subject.value) {
                push = false;
            }
        }

        if (push) {
            this.annotations[annId].bindings.push(binding);
        }
    }

	/**
	 * Sets maximal id as AnnotationLastId
	 */
	this.setAnnotationLastId = function() {
		var maxId = -1;
		for (var i = 0; i < this.annotations.length; i++) {
			if (this.annotations[i].id > maxId) maxId = this.annotations[i].id;
		}
		this.annotationLastId = maxId;
	}
});

/**
 * Interaction with the server
 */
angular.module('app').service('AnnotationRESTService',['$http', 'appConfig', function($http, appConfig) {
    var urlBase = appConfig.url+'entry-point/';

    /**
     * Sends new annotation to server
     *
     * @param annotation = {
     *       id: '', //null
     *       prefix: '',
     *       suffix: '',
     *       exact: '',
     *       words: [], //getty urls
     *       bindings: []
     *  }
     * @returns {*}
     */
    this.add = function(annotation) {
        return $http({
            method: 'GET', url: urlBase + "annotate?exact=" + annotation.exact
            + "&annotations=" + annotation.words
            + "&prefix=" + annotation.prefix
            + "&suffix=" + annotation.suffix
            + "&id=" + annotation.id
        })

    };

    /**
     * Updates an annotation on the server
     *
     * @param annotation
     * @returns {*}
     */
    this.edit = function(annotation) {
        return $http({
            method: 'GET', url: urlBase + "updateAnnotation?exact=" + annotation.exact
                + "&annotationId=" + annotation.id
                + "&annotations=" + annotation.words
                + "&prefix=" + annotation.prefix
                + "&suffix=" + annotation.suffix
        })
    };

    /**
     * Deletes an annotation from the server
     *
     * @param annotationId
     * @returns {*}
     */
    this.del = function(annotationId) {
        return $http({
            method: 'GET', url: urlBase + "delAnnotation?annotationId=" + annotationId
        })
    };


    /**
     * Gets annotation bindings
     * //todo другие настройки поиска
     *
     * @param search a search word
     * @returns {*}
     */
    this.getBindings = function(search) {
        return  $http({
            method: 'GET', url: urlBase + "search?search=" + search.search
            + "&indexDataset=" + search.indexDataset
            + "&luceneIndex=" + search.luceneIndex
        })
    };

    /**
     * Gets binding by uri
     *
     * @param uri
     * @returns {*}
     */
    this.getBindingByURI = function(uri) {
        return  $http({
            method: 'GET', url: urlBase + "getBinding?uri=" + uri
        })
    };
}]);

/**
 * todo
 */
angular.module('app').service('BindingsService', function() {
    /**
    * var bindings = [//getty object
    *          {
    *              Parents: {
    *                  type: '',
    *                  value:''
    *               },
    *              ScopeNote: {
    *                  xml:lang: '',
    *                  type: '',
    *                  value: ''
    *              },
    *              Subject: {
    *                  type: "uri",
    *                  value: "http://vocab.getty.edu/aat/300417284"
    *              },
    *              Term: {
    *                  xml:lang: "en",
    *                  type: "literal",
    *                  value: "flat file cabinets"
    *             },
    *             humanURI: {String}
    *          }, ...
    *       ]
    * */
    this.bindings = [];
    var getty = 'http://vocab.getty.edu/';

    /**
     * Adds a binding in this.bindings
     *
     * @param binding
     * @returns {Array|null}
     */
    this.addBinding = function(binding) {
        // if binding not in list
        if (this.search(binding.Subject.value) == -1) {
            this.bindings.push(binding);

            return this.bindings;
        } else {
            return null;
        }
    };

    /**
     * Adds bindings list in this.bindings
     *
     * @param bindings
     * @returns {Array|*}
     */
    this.addBindingsList = function(bindings) {
        var i;

        for (i = 0; i < bindings.length; i++) {
            this.addBinding(bindings[i]);
        }

        return this.bindings;
    };

    /**
     * Search a binding in this.bindings by uri
     *
     * @param uri
     * @returns {number} index of binding | -1 if not found
     */
    this.search = function(uri) {
        //url starts with getty string
        if (uri.indexOf(getty) == 0) {
            var i;

            for (i = 0; i < this.bindings.length; i++) {
                if (this.bindings[i].Subject.value === uri) {
                    return i;
                }
            }
        }
        return -1;
    };

    /**
     * Gets the binding by uri
     *
     * @param uri
     * @returns {Array|null}
     */
    this.getBinding = function(uri) {
        var id = this.search(uri);

        if (id != -1) {
            return this.bindings[id];
        }

        return null;
    };



    this.insertHumanURI = function(bindings) {

        var getHumanURI = function(binding) {
            var uriArray = binding.Subject.value.split("/");
            var indexDataset = uriArray[3];
            var bindingID = uriArray[4];

            var humanURI = "";
            switch(indexDataset) {
                case 'aat':
                    humanURI = "http://www.getty.edu/vow/AATFullDisplay?find=&logic=AND&note=&subjectid=" + bindingID;
                    break;
                case 'tgn':
                    humanURI = "http://www.getty.edu/vow/TGNFullDisplay?find=&place=&nation=&english=Y&subjectid=" + bindingID;
                    break;
                case 'ulan':
                    humanURI = "http://www.getty.edu/vow/ULANFullDisplay?find=&role=&nation=&subjectid=" + bindingID;
                    break;
                default:
            }

            return humanURI;
        };

        if (angular.isArray(bindings)) {
            angular.forEach(bindings, function(binding) {
                binding.humanURI = getHumanURI(binding);
            });
        } else {
            bindings.humanURI = getHumanURI(bindings);
        }

        return bindings;
    };
});

/**
 * todo
 */
angular.module('app').service('AnnotationBoxService', function(AnnotationsService) {
    /**
     * Creates annotationBox
     *
     * @param id annotation id
     * @param textEl dom node or selected text
     * @param isRange true if textEl is selected text
     */
    this.updateBox = function(id, textEl, isRange) {
        if (isRange === undefined) {
            isRange = false;
        }

        // create annotation box
        var annotation = AnnotationsService.getAnnotation(id);
        var el = angular.element('<annotation-box annotationId="' + annotation.id +
            '" edit="editAnnotation(' + annotation.id + ', $event)"></annotation-box>');

        if (isRange) {
            textEl.deleteContents();
            textEl.insertNode(el[0]);
        } else {
            var rng = document.createRange();
            rng.selectNode(textEl);
            rng.deleteContents();
            rng.insertNode(el[0]);
        }
    };

    /**
     * Creates temporary boxes for highlighting duplicates of search words
     *
     * @param word search words
     * @param parentEl the parent element in which the duplicates are searched for
     */
    this.selectRepeats = function(word, parentEl) {
        /**
         * Highlights duplicates of search words in childNode of parentEl
         *
         * @param nodeId id of parentEl childNode
         * @param parentEl the parent element in which the duplicates are searched for
         * @param word search words
         */
        var select = function(nodeId, parentEl, word) {
            var pos = 0;
            var i = 0;

            var node = parentEl.childNodes[nodeId];
            var text = node.textContent;

            word = word.toLowerCase();
            while (true) {
                var foundPos = text.toLowerCase().indexOf(word, pos);

                if (foundPos == -1) break;

                //create range with found word
                var rng = document.createRange();
                rng.setStart(node, foundPos);
                rng.setEnd(node, foundPos + word.length);


                //create element for surround
                var el = document.createElement('span');
                el.setAttribute("class", "temp-selection");
                el.setAttribute("ng-click", "delRepeat($event)");

                rng.surroundContents(el);

                i++;
                nodeId = nodeId + 2;
                node = parentEl.childNodes[nodeId];
                text = node.textContent;
            }
        };

        var lastChildNodeId = parentEl.childNodes.length;
        if (word.length > 0) {
            for (var j = lastChildNodeId - 1; j > -1; j = j -2) {
                select(j, parentEl, word);
            }
        }
    };

    /**
     * Deletes the temporary box for highlighting duplicates of search words
     *
     * @param el the temporary box
     */
    this.delRepeat = function(el) {
        var parent = el.parentNode;
        var text = document.createTextNode(el.textContent);

        parent.insertBefore(text, el);
        el.remove();

        parent.normalize();
    };

    /**
     * Deletes all highlighting repeats
     */
    this.delAllRepeats = function() {
        var elements = document.getElementsByClassName('temp-selection');

        while (elements.length > 0) {
            this.delRepeat(elements[0]);
            elements = document.getElementsByClassName('temp-selection');
        }
    };

    /**
     * Deletes the annotation box
     *
     * @param el the annotation box
     */
    this.delBox = function(el) {
        var parent = el.parentNode;

        var text = document.createTextNode(el.firstChild.firstChild.nodeValue);

        var rng = document.createRange();
        rng.selectNode(el);
        rng.deleteContents();
        rng.insertNode(text);

        parent.parentNode.normalize();
    }
});

//todo file sevice
