package de.uni_passau.fim.semantic_web.annotations.text.server.rest.Util;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLEncoder;

/**
 *
 */
public class Getty {
	/**
	 * Forms uri for request to Getty
	 *
	 * @param search word for search
	 * @param lucene full or brief search
	 * @param dataset AAT, TGN, ULAN
	 * @return string with uri
	 */
	public static String getURI(String search, String lucene, String dataset) {
		String uri = "";
		try {
			String q;
			if (dataset.equals("any")) {
				q = "" +
					"select ?Subject ?Term ?Parents ?ScopeNote { ?Subject a skos:Concept;" + lucene +
					" \"" + search + "\"; gvp:prefLabelGVP/xl:literalForm ?Term." +
					"optional {?Subject gvp:parentStringAbbrev ?Parents}" +
					"optional {?Subject skos:scopeNote [dct:language gvp_lang:en; rdf:value ?ScopeNote]}}";
			} else {
				q = "" +
					"select ?Subject ?Term ?Parents ?ScopeNote { ?Subject a skos:Concept;" + lucene +
					" \"" + search + "\";skos:inScheme "
					+ dataset
					+ ": ; gvp:prefLabelGVP/xl:literalForm ?Term." +
					"optional {?Subject gvp:parentStringAbbrev ?Parents}" +
					"optional {?Subject skos:scopeNote [dct:language gvp_lang:en; rdf:value ?ScopeNote]}}";
			}

			String query = URLEncoder.encode(q, "UTF-8");
			uri = "http://vocab.getty.edu" + "/sparql.json?query=" + query;


		} catch (Exception exc) {
			System.out.println(exc.toString());
		}
		return uri;
	}

	/**
	 * Makes request to Getty
	 *
	 * @param uri
	 * @return Getty responce
	 */
	public static String getGettyResponce(String uri) {
		String textView = "";
		StringBuilder stringBuilder = new StringBuilder();
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(uri);
			request.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8");
			request.addHeader("Accept-Language  ", "ru,en-us;q=0.7,en;q=0.3");
			request.addHeader("Accept-Charset", "windows-1251, utf-8;q=0.7,*;q=0.7");

			HttpResponse response = client.execute(request);

			// Get the response
			BufferedReader rd = new BufferedReader
				(new InputStreamReader(
					response.getEntity().getContent()));

			String line;

			while ((line = rd.readLine()) != null) {
				stringBuilder.append(line);
				textView.concat(line);
			}
		} catch (Exception exc) {
			System.out.println("error");
			System.out.println(exc.toString());
		}

		return stringBuilder.toString();
	}
}
