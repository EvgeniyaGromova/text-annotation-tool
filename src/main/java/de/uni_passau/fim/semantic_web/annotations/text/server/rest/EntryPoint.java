package de.uni_passau.fim.semantic_web.annotations.text.server.rest;


import de.uni_passau.fim.semantic_web.annotations.text.server.rest.Util.FileUtil;
import de.uni_passau.fim.semantic_web.annotations.text.server.rest.Util.Getty;
import de.uni_passau.fim.semantic_web.annotations.text.server.rest.Util.JSONAnnotation;
import org.apache.commons.io.FilenameUtils;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.server.mvc.Viewable;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


@Path("/entry-point")
@Singleton
public class EntryPoint {
	/**
	 * saves name of uploaded file
	 */
	private String fileName;

	/**
	 * saves all annotations
	 */
	private JSONArray annotationsList = new JSONArray();

	@GET
	@Produces(MediaType.TEXT_HTML)
	public Viewable index() {
		return new Viewable("/index", null);
	}

	/**
	 * Receives list of files, finds a txt file and json file with the name "name_of_txt_file_ann.json"
	 *
	 * @param tags file tags
	 * @param bodyParts list of files
	 * @param fileDispositions file dispositions
	 * @param response for set up response
	 * @param header for set up headers
	 * @return {
	 *     annotation: [list of annotations]
	 *     bindings: [list of bindings]
	 *     filename: String
	 *     text: content of text file
	 * }
	 */
	@POST
	@Path("files")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadFiles(@DefaultValue("") @FormDataParam("tags") String tags,
								@FormDataParam("files") List<FormDataBodyPart> bodyParts,
								@FormDataParam("files") FormDataContentDisposition fileDispositions,
								@Context HttpServletResponse response,
								@Context HttpHeaders header) {
		setResponse(response);

		FileUtil.sortFilesList(bodyParts);

		FormDataBodyPart txtFile = FileUtil.getTXTFile(bodyParts);
		if (txtFile == null) {
			return Response.status(400).entity("{\"error\": \"txt file not found\"}").build();
		}

		JSONArray output = new JSONArray();
		annotationsList = new JSONArray();

		BodyPartEntity txtEntity = (BodyPartEntity) txtFile.getEntity();
		/*
	  saves content of uploaded file
	 */
		String fileContent = FileUtil.getFileContent(txtEntity.getInputStream());
		fileName = FilenameUtils.getBaseName(txtFile.getContentDisposition().getFileName());

		JSONObject file = new JSONObject();
		file.put("filename", fileName);
		file.put("text", fileContent);

		FormDataBodyPart jsonFile = FileUtil.getFileByName(bodyParts, fileName + "_ann.json");
		if (jsonFile != null) {
			BodyPartEntity jsonEntity = (BodyPartEntity) jsonFile.getEntity();
			String annotationString = FileUtil.getFileContent(jsonEntity.getInputStream());

			JSONParser parser = new JSONParser();
			try {
				Object json = parser.parse(annotationString);
				file.put("annotation", json);
				annotationsList = new JSONArray(annotationString);
				List<JSONObject> bindings = JSONAnnotation.getAllAnnotationBindings(annotationsList);
				file.put("bindings", new JSONArray(bindings));
			} catch (ParseException e) {
				e.printStackTrace();
				return Response.status(400).entity("{\"error\" \"can not parse file with annotations\"}").build();
			}
		}
		output.put(file);

		return Response.status(200).entity(output.toString()).build();
	}

	/**
	 * Returns Getty binding by uri
	 *
	 * @param uri getty uri
	 * @param response for set up response
	 * @param header for set up headers
	 * @return response from getty
	 */
	@GET
	@Path("getBinding")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String getBinding(@QueryParam("uri") String uri,
							 @Context HttpServletResponse response,
							 @Context HttpHeaders header) {
		setResponse(response);
		return JSONAnnotation.getBinding(uri);
	}

	/**
	 * Gets Getty response to find all bindings
	 *
	 * @param search word for annotation
	 * @param indexDataset Getty vocabularies: AAT, TGN, ULAN
	 * @param luceneIndex brief or full
	 * @param response for set up response
	 * @param header for set up headers
	 * @return response from getty
	 */
	@GET
	@Path("search")
	@Produces(MediaType.APPLICATION_JSON)
	public String getVocabulary(@QueryParam("search") String search,
								@DefaultValue("aat") @QueryParam("indexDataset") String indexDataset,
								@DefaultValue("brief") @QueryParam("luceneIndex") String luceneIndex,
								@Context HttpServletResponse response,
								@Context HttpHeaders header) {
		setResponse(response);

		LuceneIndex lucIndex = LuceneIndex.getIndex(luceneIndex);
		String lucene;
		switch (lucIndex) {
			case BRIEF:
				lucene = "luc:term";
				break;
			case FULL:
				lucene = "luc:text";
				break;
			default:
				lucene = "luc:term";
		}
		IndexDataset dataset = IndexDataset.getDataset(indexDataset);

		String uri = Getty.getURI(search, lucene, dataset.getValue());
		return Getty.getGettyResponce(uri);
	}

	/**
	 * Creates new annotation
	 *
	 * @param annotationURIs new bindings
	 * @param id annotation id
	 * @param exact annotated word
	 * @param suffix suffix annotated word
	 * @param prefix prefix annotated word
	 * @param response for set up response
	 * @param header for set up headers
	 * @return Status 200 with annotation id
	 */
	@GET
	@Path("annotate")
	@Produces(MediaType.APPLICATION_JSON)
	public Response annotate(@QueryParam("id") Integer id,
							 @QueryParam("annotations") String annotationURIs,
							 @QueryParam("exact") String exact,
							 @DefaultValue("") @QueryParam("suffix") String suffix,
							 @DefaultValue("") @QueryParam("prefix") String prefix,
							 @Context HttpServletResponse response,
							 @Context HttpHeaders header) {

		setResponse(response);
		//annotationLastId = id;
		if (exact.length() > 0) {
			JSONArray targets = JSONAnnotation.createTargets(annotationURIs, prefix, suffix, exact);

			//create annotation
			JSONObject annotation = JSONAnnotation.createAnnotation(id);

			annotation.put("target", targets);

			//save annotation to list
			annotationsList.put(annotation);

			return Response.status(200).entity(String.valueOf(id)).build();
		} else {
			return Response.status(500).entity("{\"error\": \"annotation is empty\"}").build();
		}
	}


	/**
	 * Updates annotation with new bindings
	 *
	 * @param annotationURIs new bindings
	 * @param annotationId annotation id
	 * @param exact annotated word
	 * @param suffix suffix annotated word
	 * @param prefix prefix annotated word
	 * @param response for set up response
	 * @param header for set up headers
	 * @return Status 200
	 */
	@GET
	@Path("updateAnnotation")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateAnnotation(@QueryParam("annotations") String annotationURIs,
									 @QueryParam("annotationId") Integer annotationId,
									 @QueryParam("exact") String exact,
									 @DefaultValue("") @QueryParam("suffix") String suffix,
									 @DefaultValue("") @QueryParam("prefix") String prefix,
									 @Context HttpServletResponse response,
									 @Context HttpHeaders header) {
		setResponse(response);

		int index = JSONAnnotation.getAnnotationIndexById(annotationsList, annotationId);
		if (index >= 0) {
			JSONObject annotation = (JSONObject) annotationsList.get(index);

			//The datetime must be a xsd:dateTime with the UTC timezone expressed as "Z".
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
			Date date = new Date();

			annotation.put("modified", dateFormat.format(date));

			JSONArray targets = JSONAnnotation.createTargets(annotationURIs, prefix, suffix, exact);
			annotation.put("target", targets);

			return Response.status(200).entity("{}").build();
		} else {
			return Response.status(404)
				.entity("{\"error\": \"annotation with id not " + annotationId + " found\"}").build();
		}
	}


	/**
	 * Removes annotation from annotationsList by id
	 *
	 * @param annotationId annotationId
	 * @param response for set up response
	 * @param header for set up headers
	 * @return Status 200
	 */
	@GET
	@Path("delAnnotation")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delAnnotation(@QueryParam("annotationId") Integer annotationId,
								  @Context HttpServletResponse response,
								  @Context HttpHeaders header) {
		setResponse(response);

		int index = JSONAnnotation.getAnnotationIndexById(annotationsList, annotationId);
		if (index >= 0) {
			annotationsList.remove(index);

			return Response.status(200).entity("{}").build();
		} else {
			return Response.status(404)
				.entity("{\"error\": \"annotation with id not " + annotationId + " found\"}").build();
		}
	}

	/**
	 * Saves annotation file with name "txt_file_name_ann.json" for downloading
	 *
	 * @param response for set up response
	 * @param header for set up headers
	 * @return Status 200 with file or Status 500 with saving file error
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("saveFile")
	public Response saveFile(@Context HttpServletResponse response,
							 @Context HttpHeaders header) {
		setResponse(response);

		File file;
		try {
			file = FileUtil.saveToFile(annotationsList.toString(),fileName + "_ann.json");
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(500).entity("{\"error\": \"saving file error\"}").build();
		}
		return Response.status(200)
			.entity(file)
			.header("Content-Disposition", "attachment; filename=" + file.getName())
			.build();
	}

	/**
	 * Sets headers to response
	 * @param response for set up headers for response
	 */
	private void setResponse(HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");
	}

}
