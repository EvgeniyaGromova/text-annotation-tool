package de.uni_passau.fim.semantic_web.annotations.text.server.rest.Util;

import org.apache.commons.io.FilenameUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import java.io.*;
import java.util.Comparator;
import java.util.List;

public class FileUtil {
	public static void sortFilesList(List<FormDataBodyPart> bodyParts) {
		bodyParts.sort(Comparator.comparing(o -> o.getContentDisposition().getFileName()));
	}

	/**
	 * Reads the file and returns the file include
	 *
	 * @param uploadedInputStream
	 * @param uploadedFileLocation location for saving file if saveInFile true
	 * @param saveInFile           true if need to save file
	 * @return
	 */
	public static void writeToFile(InputStream uploadedInputStream,
							   String uploadedFileLocation,
							   Boolean saveInFile) {
		/*StringBuilder stringBuilder = new StringBuilder();
		try {
			BufferedReader rd = new BufferedReader
				(new InputStreamReader(
					uploadedInputStream));
			String line;

			while ((line = rd.readLine()) != null) {
				stringBuilder.append(line);
			}*/
			if (saveInFile) {
				int read;// = 0;
				byte[] bytes = new byte[1024];

				//save in the file
				OutputStream out = null;
				try {
					out = new FileOutputStream(new File(uploadedFileLocation));
					while ((read = uploadedInputStream.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }

					out.flush();
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}



			}


		/*} catch (IOException e) {

			e.printStackTrace();
		}*/

		//String output = stringBuilder.toString();
		//output = output.replaceAll("[\\x00-\\x1F]", " ").replaceAll("[\\s]{2,}", " ");

		//return output;
	}

	public static File saveToFile (String content, String name) throws IOException {
		//save to file
		BufferedWriter bw = null;
		FileWriter fw = null;
		String fileName  = System.getProperty("user.dir") + File.separator + "download" + File.separator + name;
		System.out.println(fileName);

		//String content = annotationsList.toString();
		fw = new FileWriter(fileName);
		bw = new BufferedWriter(fw);
		bw.write(content);

		if (bw != null)
			bw.close();
		if (fw != null)
			fw.close();

		return new File(fileName);
	}

	public static String getFileContent(InputStream uploadedInputStream) {
		StringBuilder stringBuilder = new StringBuilder();
		try {
			BufferedReader rd = new BufferedReader
				(new InputStreamReader(
					uploadedInputStream));
			String line;

			while ((line = rd.readLine()) != null) {
				stringBuilder.append(line);
			}
		} catch (IOException e) {

			e.printStackTrace();
		}

		String output = stringBuilder.toString();
		output = output.replaceAll("[\\x00-\\x1F]", " ").replaceAll("[\\s]{2,}", " ");

		return output;
	}

	/**
	 * todo
	 * @param bodyParts
	 * @return
	 */
	public static FormDataBodyPart getTXTFile(List<FormDataBodyPart> bodyParts) {
		for (int i = 0; i < bodyParts.size(); i++) {
			String fullFileName = bodyParts.get(i).getContentDisposition().getFileName();

			if (FilenameUtils.getExtension(fullFileName).equals("txt")) {
				return bodyParts.get(i);
			}
		}
		return null;
	}

	public static FormDataBodyPart getFileByName(List<FormDataBodyPart> bodyParts, String fileName) {
		for (int i = 0; i < bodyParts.size(); i++) {
			if (bodyParts.get(i).getContentDisposition().getFileName().equals(fileName)) {
				return bodyParts.get(i);
			}
		}
		return null;
	}
}
