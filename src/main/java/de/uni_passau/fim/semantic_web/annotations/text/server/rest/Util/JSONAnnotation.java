package de.uni_passau.fim.semantic_web.annotations.text.server.rest.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Contains functions for creation annotation in format: https://www.w3.org/TR/annotation-model/
 * Additionally has function for getting binding JSON by uri from Getty response
 */
public class JSONAnnotation {
	/**
	 * Creates targets for annotation in format 4.2.4 Text Quote Selector
	 *
	 * @param annotationURIs uri for targets; separator ","
	 * @param prefix a snippet of text that occurs immediately before the text which is being selected.
	 * @param suffix the snippet of text that occurs immediately after the text which is being selected.
	 * @param exact a copy of the text which is being selected, after normalization
	 * @return JSONArray with targets
	 */
	public static JSONArray createTargets(String annotationURIs, String prefix, String suffix, String exact) {
		JSONArray targets = new JSONArray();
		String[] URIs = annotationURIs.split(",");
		for (String uri : URIs) {
			JSONObject target = new JSONObject();
			target.put("source", uri);

			JSONObject selector = new JSONObject();
			selector.put("type", "TextQuoteSelector");
			selector.put("exact", exact);
			if (prefix.length() > 0) {
				selector.put("prefix", prefix);
			}
			if (suffix.length() > 0) {
				selector.put("suffix", suffix);
			}

			target.put("selector", selector);

			targets.put(target);
		}

		return targets;
	}

	/**
	 * Creates list of bindings from getty response by list of bindings uri
	 *
	 * @param annotations annotations list
	 * @return list of responses from getty
	 */
	public static List<JSONObject> getAllAnnotationBindings(JSONArray annotations) {
		List<JSONObject> bindings = new ArrayList<>();
		for (int i = 0; i < annotations.length(); i++) {
			JSONArray target = annotations.getJSONObject(i).getJSONArray("target");
			for (int j = 0; j < target.length(); j++) {
				String uri = target.getJSONObject(j).getString("source");
				bindings.add(new JSONObject(JSONAnnotation.getBinding(uri)));
			}
		}

		return bindings;
	}

	/**
	 * Gets binding by uri from getty response
	 *
	 * @param uri bindings uri
	 * @return getty response
	 */
	public static String getBinding(String uri) {
		String[] uriArray = uri.split("/");
		String url = Getty.getURI(uriArray[uriArray.length - 1], "luc:text", "any");
		return Getty.getGettyResponce(url);
	}

	/**
	 * Create annotation without bindings. Format: https://www.w3.org/TR/annotation-model/
	 *
	 * @param id id of new annotation
	 * @return annotation
	 */
	public static JSONObject createAnnotation(Integer id) {
		//The datetime must be a xsd:dateTime with the UTC timezone expressed as "Z".
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		Date date = new Date();

		JSONObject annotation = new JSONObject();

		annotation.put("@context", "http://www.w3.org/ns/anno.jsonld");
		annotation.put("id", id);
		annotation.put("type", "Annotation");
		annotation.put("creator", "Anonym");
		annotation.put("created", dateFormat.format(date));

		return annotation;
	}

	/**
	 * Returns index of annotation by id
	 *
	 * @param annotations annotations list
	 * @param id annotation id
	 * @return index in annotation list or -1, if not found
	 */
	public static int getAnnotationIndexById(JSONArray annotations, Integer id) {
		for (int i = 0; i < annotations.length(); i++) {
			int annotationId = annotations.getJSONObject(i).getInt("id");
			if (annotationId == id) return i;

		}
		return -1;
	}
}