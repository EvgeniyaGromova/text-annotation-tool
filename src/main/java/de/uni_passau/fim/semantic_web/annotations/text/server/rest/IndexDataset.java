package de.uni_passau.fim.semantic_web.annotations.text.server.rest;

/**
 * For for check IndexDataset from request.
 * Saves Getty Vocabularies AAT, TGN, ULAN
 */
public enum IndexDataset {

	AAT("aat"),
	TGN("tgn"),
	ULAN("ulan");

	/**
	 * Saves actual value
	 */
	private String value;

	/**
	 * Constructor
	 *
	 * @param type aat, tgn or ulan
	 */
	IndexDataset(String type) {
		this.value = type;
	}

	/**
	 * Return compare index or AAT default
	 *
	 * @param pType string fot search compare index
	 * @return compare index or AAT default
	 */
	static public IndexDataset getDataset(String pType) {
		//System.out.println("IndexDataset " + pType);
		for (IndexDataset type : IndexDataset.values()) {
			if (type.getValue().equals(pType)) {

				return type;
			}
		}
		return AAT;
	}

	/**
	 * Return actual value
	 * @return actual value
	 */
	public String getValue() {
		return this.value;
	}
}