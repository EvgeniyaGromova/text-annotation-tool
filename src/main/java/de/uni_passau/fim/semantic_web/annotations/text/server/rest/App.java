package de.uni_passau.fim.semantic_web.annotations.text.server.rest;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.servlet.ServletContainer;



public class App {
    /**
     * Main class for starting jetty server
     *
     * @param args no args
     */
    public static void main(String[] args) {

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        Server jettyServer = new Server(8080);
        jettyServer.setHandler(context);

        ServletHolder jerseyServlet = context.addServlet(ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);

        String providerClassName = MultiPartFeature.class.getCanonicalName() + ", " +
                EntryPoint.class.getCanonicalName();
        jerseyServlet.setInitParameter(ServerProperties.PROVIDER_CLASSNAMES, providerClassName);

        try {
            jettyServer.start();
            jettyServer.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}