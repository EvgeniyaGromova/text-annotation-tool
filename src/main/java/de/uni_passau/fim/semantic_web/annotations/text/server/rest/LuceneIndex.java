package de.uni_passau.fim.semantic_web.annotations.text.server.rest;

/**
 * For for check LuceneIndex from request.
 * Saves two indexes for searching term.
 * BRIEF (luc:term): includes all terms (prefLabels and altLabels) and subject ID (default)
 * FULL (luc:text): includes all terms, qualifiers, subject ID, and scope notes.
 */
public enum LuceneIndex {
    BRIEF("brief"),
    FULL("full");

    /**
     * Saves actual value
     */
    private String value;

    /**
     * Constructor
	 *
     * @param type brief or full
     */
    LuceneIndex(String type) {
        this.value = type;
    }

    /**
     * Return compare index or BRIEF default
	 *
     * @param pType string fot search compare index
     * @return compare index or BRIEF default
     */
    static public LuceneIndex getIndex(String pType) {
        for (LuceneIndex type : LuceneIndex.values()) {
            if (type.getValue().equals(pType)) {
                return type;
            }
        }
        return BRIEF;
    }

    /**
     * Return actual value
     * @return actual value
     */
    public String getValue() {
        return this.value;
    }

}
